;
;   \Author   Hans Kramer
;
;   \Date     June 2018
;
;   Hardware  PICAXE-08M2 (from Julia's snap circuit kit)
;             Three 8x8 dot matrix LEDS display driven by three maxim MAX7219


; registers
symbol cnt   = b0
symbol cnt2  = b1
symbol param = w1
symbol param_low  = b2
symbol param_high = b3
symbol mask  = w2
symbol work  = w3
symbol addr  = w4
symbol cnt3  = w5
symbol addr2 = w8

; pins
symbol CLK   = B.1
symbol DIN   = B.2
symbol LOAD  = B.4

; 7219 constants
symbol test_pixels     = %0000111100000001
symbol test_pixels_off = %0000111100000000
symbol shutdown_off    = 0x0c01
symbol decode_off      = 0x0900
symbol scan_all        = 0x0b07


; Tera Volts
data 100, (%00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000)
data 108, (%10000000, %10000000, %11111110, %10000000, %10011100, %10101010, %00101010, %00011010)
data 116, (%00000000, %00111110, %00100000, %00100000, %00010000, %00000000, %00001100, %00101010)
data 124, (%00101010, %00011110, %00000000, %11111000, %00000100, %00000010, %00000100, %11111000)
data 132, (%00000000, %00011100, %00100010, %00100010, %00011100, %00000000, %10000010, %11111110)
data 140, (%00000010, %00100000, %11111100, %00100010, %00000010, %00000000, %00011010, %00101010)
data 148, (%00101010, %00101100, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000)
data 156, (%00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000)
data 164, (%00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000)
data 172, (%00000000, %00000000)



main: 
    ; set clock to 32 MHz
    setfreq m32
    low LOAD
    low CLK
    low DIN

    ; flush
    gosub send_nop
    gosub send_nop
    gosub send_nop
    pulsout LOAD, 1

    ; test pixels
    let   param = test_pixels
    gosub send_word
    gosub send_word
    gosub send_word
    pulsout LOAD, 1

    ; test pixels off
    let   param = test_pixels_off
    gosub send_word
    gosub send_word
    gosub send_word
    pulsout LOAD, 1

    ; come out off shutdown mode
    let   param = shutdown_off
    gosub send_word
    gosub send_word
    gosub send_word
    pulsout LOAD, 1

    ; decode mode off, address every pixel
    let   param = decode_off
    gosub send_word
    gosub send_word
    gosub send_word
    pulsout LOAD, 1

    ; enable all 8 rows
    let   param = scan_all
    gosub send_word
    gosub send_word
    gosub send_word
    pulsout LOAD, 1

    ; set min intensity
    let   param = %0000101000000001
    gosub send_word
    gosub send_word
    gosub send_word
    pulsout LOAD, 1

text_loop:
    for cnt3 = 100 to 173
        let   param = cnt3
        gosub load_screen
    next cnt3
    pause 3000
    goto text_loop
    
    end


load_screen:
    let addr = param + 1
    for cnt2 = 1 to 8
        ; write to right most matrix, filing from right to left
        let  param_high = cnt2
        let  addr2      = addr - cnt2 
        read addr2, param_low
        gosub send_word

        ; write to middle matrix, filing from left to right (opposite direction!!!)
        ;                         reducing tearing effect
        let  param_high = 9 - cnt2
        let  addr2      = addr - 17 + cnt2
        read addr2, param_low
        gosub send_word

        ; write to most left matrixm filing from right to left 
        let  param_high = cnt2
        let  addr2      = addr - 16 - cnt2 
        read addr2, param_low
        gosub send_word
        pulsout LOAD, 1
    next cnt2
    return


; clearing all three matrices
clear_display:
    let param_low = %00000001
    for param_high = 1 to 8
        gosub send_word
        gosub send_word
        gosub send_word
        let param_low = %00000000
        pulsout LOAD, 1
    next param_high
    return


; send word, 08M2 does not have shift nor shiftout ... so this is slooooow
; note : pulsout LOAD has to be called afterwards
send_word:
    low DIN
    let work = param
    for cnt = 0 to 15
        let mask  = work & %1000000000000000
        if mask = %1000000000000000 then
            high DIN
        else
            low DIN
        endif
        pulsout CLK, 1
        let work=work*2
    next cnt
    low DIN
    return


; send nop command
; note : pulsout LOAD has to be called afterwards
send_nop:
    low DIN
    for cnt = 0 to 15
        pulsout CLK, 1
    next cnt
    return
