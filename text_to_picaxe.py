#! /usr/bin/python
import sys

text_data = """
        111111             1   1      11  1                     
          1                1   1       1  1                     
          1  11  111   11  1   1  11   1 111   111              
          1 1  1 1  1    1 1   1 1  1  1  1   1                 
          1 1111 1    1111 1   1 1  1  1  1   1111              
          1 1    1    1  1  1 1  1  1  1  1      1              
          1  111 1     111   1    11  111  11 111               
                                                                
"""

data = []
tmp = text_data.split("\n")[1:-1]
for x in tmp:
    data += [[x[i:i+8] for i in range(0, len(x), 8)]]

lin_data = [[] for i in range(len(data[0]))]
for y in range(len(data[0])):
    for x in range(8):
        lin_data[y]+=[data[x][y].replace(' ', '0')]

count = 100
for line in lin_data:
    o = ['%'] * 8
    for i in range(8):
        for j in range(8):
            o[i] += line[j][i]
    print "data %d, (" % count + ", ".join(o) + ")"
    count += 8

sys.exit(0)
